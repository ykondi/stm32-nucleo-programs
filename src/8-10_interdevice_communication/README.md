Inter-device Communication
==========================

An important aspect of many embedded applications is having the ability to
communicate between multiple microcontrollers (or circuits in general).

Illustrated here are two popular protocols: i2c and SPI.
