/*
 * This program illustrates master-slave SPI communication
 * The user button can be used to toggle between requesting accelerometer and
 * temperature values.
 */
#include "mbed.h"
 
SPI device(SPI_MOSI, SPI_MISO, SPI_SCK);
DigitalOut tr(PA_8);
DigitalIn mybutton(USER_BUTTON);

Serial pc(SERIAL_TX, SERIAL_RX);
 
int main() {
    int toggle = 0, dummy, y, z, x;
    while(1) {
        if (!mybutton) toggle = !toggle;
        pc.printf("Requesting ");
        if (toggle) { tr = 0; pc.printf("temperature: %d\n\r", device.write(1)); tr = 1;}
        else {
	    tr = 0;
            pc.printf("accelerometer x value: %d\n\r", device.write(0));
	    tr = 1;
        }
        wait(0.5);
    }
}
 
