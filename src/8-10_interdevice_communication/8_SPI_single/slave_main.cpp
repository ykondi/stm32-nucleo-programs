#include "mbed.h"
#include "rtos.h"

 // Reply to a SPI master as slave

 
SPISlave device(SPI_MOSI, SPI_MISO, SPI_SCK, PA_4); // mosi, miso, sclk, ssel
Serial pc(SERIAL_TX, SERIAL_RX);
AnalogIn X(PA_0), Y(PA_1), Z(PB_0);
AnalogIn tempr(A4);
AnalogIn photo(A5);
 
float mapper(float val) {return (val - 0.35) ;}
 
void AccelerometerControl(void const *args) {
    float x, y, z;
    int** arr = (int **) args;
    int* axes = *arr;
    while (1) {
       // Read data from 3 axes
       axes[0] = 1000*mapper(X.read());
       axes[1] = 1000*mapper(Y.read());
       axes[2] = 1000*mapper(Z.read());
       wait(0.2);     
    }
}

// Function to constantly keep track of the average temperature over the last
// 1s, sampled uniformly every 0.1s
void TemperatureControl(void const *args) {
    int arr[10] = {0};
    int counter = 0;
    int avg;
    int* loc = (int *)(args);
    while (1) {
        arr[counter] = (int)((3300 * tempr.read_u16()) / 0xffff) - 500;
        avg += arr[counter]/10;
        avg -= arr[(++counter)%10] / 10;
        *loc = avg;
        counter = counter % 10;
        wait(0.1);
    }
}
 
int main() {
     int *axes[3];
     int *temp;
     axes[0] = (int *)malloc(sizeof(int) * 3);
     axes[1] = axes[0] + 1;
     axes[2] = axes[1] + 1;
     temp = (int *)malloc(sizeof(int));
     Thread AccelerometerThread (AccelerometerControl, (void *)axes);
     Thread TemperatureThread (TemperatureControl, (void *)temp);
     
     device.reply(0x00);              // Prime SPI with first reply
     while(1) {
         if(device.receive()) {
             int v = device.read();   // Read byte from master
             // Print accelerometer and temperature values to the screen as a
	     // reference
             pc.printf("Acc: %d %d %d\tTemp: %d\n\r", *(axes[0]), *(axes[1]), *(axes[2]), *temp);
	     // Return temperature or acc. x-value based on request from master
             if (v == 1) device.reply(*temp - 150);
             else if (v == 0) device.reply(*(axes[0]));
             wait_us(1000);
         }
     }
 }

