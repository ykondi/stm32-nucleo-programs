SPI Communication between two devices
=====================================

SPI is a basic, intuitive protocol for communication between devices. It
requires a clock for synchronization, MISO and MOSI ports for data transfer,
and SSEL for selecting the slave being communicated with.
