SPI Multiple
============

Notice that in the previous program, there is a provision for SSEL. It serves
as a selection bit for the case when there are multiple slaves that need to
be communicated with.
