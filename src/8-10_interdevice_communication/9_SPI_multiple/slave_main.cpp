/*
 * This program shows a generic SPI slave that responds when SSEL is LOW.
 */
#include "mbed.h"
#include "rtos.h"


 
SPISlave device(SPI_MOSI, SPI_MISO, SPI_SCK, PA_4); // mosi, miso, sclk, ssel
Serial pc(SERIAL_TX, SERIAL_RX);
AnalogIn X(PA_0), Y(PA_1), Z(PB_0);
AnalogIn tempr(A4);
 
void TemperatureControl(void const *args) {
    int arr[10] = {0};
    int counter = 0;
    int avg;
    int* loc = (int *)(args);
    while (1) {
        arr[counter] = (int)((3300 * tempr.read_u16()) / 0xffff) - 500;
        avg += arr[counter]/10;
        avg -= arr[(++counter)%10] / 10;
        *loc = avg;
        counter = counter % 10;
        wait(0.1);
    }
}
 
int main() {
     int *temp;
     temp = (int *)malloc(sizeof(int));
     Thread TemperatureThread (TemperatureControl, (void *)temp);
     
     device.reply(0x00);              // Prime SPI with first reply
     while(1) {
         if(device.receive()) {
             int v = device.read();   // Read byte from master
             if (v == 1) device.reply(*temp - 150);
             pc.printf("sending\n\r");
             wait_us(10);
         }
         pc.printf("stopped\n\r");
     }
}

