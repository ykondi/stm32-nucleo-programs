/*
 * This program illustrates how to communicate with multiple slaves using SPI.
 * PA_4 and PA_8 act as the selectors for the two slaves.
 */
#include "mbed.h"
 
SPI device(SPI_MOSI, SPI_MISO, SPI_SCK);

DigitalOut ssel_0(PA_4);
DigitalOut ssel_1(PA_8);

DigitalIn mybutton(USER_BUTTON);
Serial pc(SERIAL_TX, SERIAL_RX);
 
int main() {
    int i = 0;
    int toggle = 0;
    int recvd = 0;
    ssel_0 = 0; ssel_1 = 1;
    while(1) {
    	// Flip ssel_0 and ssel_1 to toggle between the two slaves
        if (!mybutton) {
            toggle = !toggle;
            ssel_0 = toggle;
            ssel_1 = !toggle;
            wait(0.2);
        }
        recvd = device.write(1) + 150;
        pc.printf("device %d, temperature %2.1f\n\r", toggle, ((float)recvd)/10);
        wait_us(50);
    }
}
 
