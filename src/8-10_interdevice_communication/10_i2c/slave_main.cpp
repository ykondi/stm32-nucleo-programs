#include "mbed.h"
#include "rtos.h"

// Program for second i2c slave
I2CSlave slave(I2C_SDA, I2C_SCL); 
Serial pc(SERIAL_TX, SERIAL_RX);
AnalogIn X(PA_0), Y(PA_1), Z(PB_0);
AnalogIn tempr(A4);
  
float mapper(float val) {return (val - 0.35) ;}
 
void readAcc(int *axes) {
       float x, y, z;
       axes[0] = 1000*mapper(X.read());
       axes[1] = 1000*mapper(Y.read());
       axes[2] = 1000*mapper(Z.read());
}


int main() {
     
     
     int axes[3];
     int temp;
     
     char buf[10];
     char choice = 'a';
     int t[] = {0,0,0};
 
     slave.address(0x62);
     while (1) {
         int i = slave.receive();
         switch (i) {
             case I2CSlave::ReadAddressed:
                 if (choice == 'a') {
                        readAcc(t);
                        slave.write((char *)t, 3*sizeof(int));
                        pc.printf("Accelerometer values %d %d %d\n\r", t[0], t[1], t[2]);
                 }
                else { 
                        temp = (int)((3300 * tempr.read_u16()) / 0xffff) - 500;
                        slave.write((char *)temp, sizeof(int));
                        pc.printf("Temperature: %2.1f\n\r", ((float)temp) / 10);
                 }
                 break;
             case I2CSlave::WriteAddressed:
                 slave.read(&choice, 1);
                 pc.printf("Request for ");
                 if (choice == 'a') pc.printf("accelerometer\n\r");
                 else pc.printf("temperature sensor\n\r");
                 break;
         }
     }
 }
