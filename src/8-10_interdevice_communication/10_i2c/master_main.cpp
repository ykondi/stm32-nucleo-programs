/*
 * Demonstrates master-slave communication by the i2c protocol.
 * Either get accelerometer values, or temperature data from slave.
 * A second slave can be added and registered with
 * a new address quite easily.
 */
#include "mbed.h"
 
I2C i2c(I2C_SDA, I2C_SCL);
 
DigitalOut myled(LED1);
DigitalIn mybutton(USER_BUTTON);
Serial pc(SERIAL_TX, SERIAL_RX);
 
int main() {
     int address = 0x62;
     char choices[] = "at";
     int axes[3];
     char data[3];
     int temp;
     int sel = 0;
     
     while (1) {
        if (!mybutton) {
            ++sel;
            i2c.write(address, choices+sel%2, 1);
            wait(0.2);
        }
        if (sel % 2 == 0) {
            // Read accelerometer data
            i2c.read(address, (char *)axes, 3*sizeof(int));
            pc.printf("Axis values: %d\t %d\t %d\n\r", axes, axes+1, axes+2);
        }
        else {
            // Read temperature data
            i2c.read(address, (char *)&temp, sizeof(int));
            pc.printf("Temperature: %2.1f C\n\r", ((float)temp)/10);
        }
        wait(0.5);
    } 
 }
