I2C Protocol
============

I2C (or *Inter-Integrated Circuit Communication*) is another protocol for
transmitting data between circuits. Unlike SPI, there is only one data line
(SDA). There's also a clock (SCK) line. Also, unlike SSEL in SPI, there is no
seperate line for addressing in I2C.
