Accelerometer Read
==================

This program demonstrates how to read from multiple AnalogIn pins at once.
Specifically, the ADXL335 accelerometer is read from.
