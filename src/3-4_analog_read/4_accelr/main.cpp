#include "mbed.h"

Serial pc(SERIAL_TX, SERIAL_RX);
AnalogIn X(PA_0), Y(PA_1), Z(PB_0);
DigitalIn mybutton(USER_BUTTON);
DigitalOut myled(LED1);

// Function to get desired range of accelerometer values. This is usually
// relative, and is advised to be determined seperately for each ADXL chip.
float mapper(float val) {return (val - 0.35) ;}

int main() {
	myled = 0;
	float x, y, z;
	while(1) {
		// Read and scale the three co-ordinate values
		x = 1000*mapper(X.read());
		y = 1000*mapper(Y.read());
		z = 1000*mapper(Z.read());
		pc.printf("%d %d %d\n\r", (int)x, (int)y, (int)z);
		wait(0.2);
    }
}
     
