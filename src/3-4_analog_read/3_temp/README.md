Temperature Sensor
==================

This program covers reading the temperature from a TMP36 temperature sensor,
scaling the value appropriately, and hence printing it to the PC.
