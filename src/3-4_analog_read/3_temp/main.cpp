/*
 * Program to demonstrate ADC.
 */

#include "mbed.h"
 
AnalogIn tempr(A1);
 
Serial pc(SERIAL_TX, SERIAL_RX);

int main() {
	unsigned int adc_val;
	float mvval;

	while(1) { 

		// Read from AnalogIn
		adc_val = tempr.read_u16();
		// Scale the value appropriately
		mvval = (float)((3300 * adc_val) / 0xffff);
		// Print this value to PC
		pc.printf("temp: %2.3f\n\r", (mvval - 500)/10);
	}
}
