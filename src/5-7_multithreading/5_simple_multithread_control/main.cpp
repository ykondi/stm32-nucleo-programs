/*
 * The purpose of this program is to show how to instantiate a thread object
 * and illustrate that the main thread runs independently of the created
 * thread.
 */

#include "mbed.h"
#include "rtos.h"
 
AnalogIn photo(A0);
AnalogIn tempr(A1);
 
DigitalOut myled(LED1);

Serial pc(SERIAL_TX, SERIAL_RX);
 
// Blink LED with a period of 2s
void led2_thread(void const *args) {
    while (true) {
        myled = !myled;
        Thread::wait(1000);
    }
}
 
int main() {
    // Instantiate Thread object with no arguments
    Thread thread(led2_thread);
    unsigned int adc_val;
    float mvval;
    float adc;
    while(1) {
	// Print temperature with period of 0.2s
        pc.printf("Tempr: %d\n\r", tempr_val);
        wait(0.2); 
        adc_val = tempr.read_u16();
        adc = tempr.read();
        mvval = (float)((3300 * adc_val) / 0xffff);
        pc.printf("adc: %f V 16bit: %hu value: %d mV\t",
            adc*3.3, adc_val, ((3300 * adc_val) / 0xffff));
            
        pc.printf("temp: %2.3f\n\r", (mvval - 500)/10);
    }
}

