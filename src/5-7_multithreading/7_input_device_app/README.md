Input Device Application
========================

This program samples accelerometer values in a separate thread and outputs them
to the PC from the main thread. This ensures that the accelerometer values are
sampled in uniform intervals, and is not subject to the unpredictable delay
caused by write operations to the PC.

On the receiving end, the PC uses these accelerometer values to control the
position of the mouse pointer. Pressing the user button on the STM32 enables
accelerometer readings to be transmitted.

The PC program to control the position of the cursor can be found in /utils .
