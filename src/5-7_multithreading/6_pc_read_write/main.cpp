/*
 * This program demonstrates how to use multithreading to simultaneously
 * publish values to the pc while still listening for user input.
 */

#include "mbed.h"
#include "rtos.h"

Serial pc(SERIAL_TX, SERIAL_RX);
DigitalIn mybutton(USER_BUTTON);
DigitalOut myled(LED1);

AnalogIn photo(A0);
AnalogIn tempr(A1);

/*
 * serial_publish runs in a separate thread and continuously publishes the
 * values obtained by dereferencing its arguments to the PC
 */
void serial_publish(void const *args) {
    int** loc = (int **) args;
    int* value = *loc;
    int* flag = *(loc + 1);
    while(1) {
        pc.printf("%d\n\r", *value);
	// If flag is 0, wait until the user enters 1
        do { Thread::wait(0.8); } while(!(*flag));
    }
}

/*
 * serial_read listens for input from the PC, and runs in a separate thread
 * so that the rest of the program can continue unhindered
 */
void serial_read(void const *args) {
    int** loc = (int **) args;
    int* flag = *(loc + 1);
    while(1) {
	// Get integer value of digit entered by the user, from the PC
        *flag = pc.getc() - 48;
        Thread::wait(0.2);
        }
    }


int main() {
    int *comm[2];
    comm[0] = (int *)malloc(sizeof(int));
    comm[1] = (int *)malloc(sizeof(int));
    *(comm[0]) = 5;
    *(comm[1]) = 0;
    Thread thread(serial_publish, (void *)comm);
    Thread thread2(serial_read, (void *)comm);
    
    bool toggle = 0;
    int to_write = 0;
    
    while (1) {
	// Check which input is requested
        if (mybutton == 0) toggle = !toggle;
        if (toggle) {
            // Read from Temp sensor
            to_write = (int)((3300 * tempr.read_u16()) / 0xffff) - 500;
            
            }
        else {
            // Read from pr
            to_write = (int)photo.read_u16();
            }
        *(comm[0]) = to_write;
        myled = toggle;
        wait(0.5);
    }
}



