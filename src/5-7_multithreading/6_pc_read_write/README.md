PC Read Write
=============

Ordinarily, one can either read or write from the pc to the ARM board
sequentially. This program demonstrates how to use multithreading to listen
for input while simultaneously relaying output to the PC.

Note that here (for linux machines), simply using cat or screen to read
data from the device is not enough. An interactive program like [cu](http://manpages.ubuntu.com/manpages/natty/man1/cu.1.html) is better
suited for the task.
