/*
	Program to test that the board is accepting and running user programs.
	Keep the LED on for 5s, turn it off for 1s.
*/
#include "mbed.h"

DigitalOut myled(LED1);

int main() {
    while(1) {
        myled = 1; // LED is ON
        wait(5.0); // 5 sec
        myled = 0; // LED is OFF
        wait(1.0); // 1 sec
    }
}

