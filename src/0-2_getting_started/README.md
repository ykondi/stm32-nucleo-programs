Getting Started
===============

The programs here form the atomic basis for the rest of the programs in this
repository. Covered here are the following:

* Blinking an LED
* Checking the User Button state
* Sending messages to the PC
