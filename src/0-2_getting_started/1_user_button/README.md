User Button
===========

The user button will be used in subsequent programs to toggle between different
options. This program serves to demonstrate that user button input is being
detected properly.
