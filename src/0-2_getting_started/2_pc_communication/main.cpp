/*
 *	This program sends the string "Hello world!", along with how long it
 *	has been running (in seconds) so that the change in text is noticeable.
 * 	Make sure to set the right baud rate at the receiving end.
 */

#include "mbed.h"

//------------------------------------
// Hyperterminal configuration
// 9600 bauds, 8-bit data, no parity
//------------------------------------

Serial pc(SERIAL_TX, SERIAL_RX);
 
DigitalOut myled(LED1);
 
int main() {
  int i = 1;
  pc.printf("Hello World !\n\r");
  while(1) { 
      wait(1);
      pc.printf("Hello world! Running for %d seconds.\n\r", i++);
      myled = !myled;
  }
}
 
