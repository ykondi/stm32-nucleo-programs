STM32 Nucleo Programs
=====================

This repository contains code to implement some simple tasks on an [STM32 Nucleo
F411RE microcontroller](http://www.st.com/web/catalog/tools/FM116/SC959/SS1532/LN1847/PF260320).

The IDE used for the same was [mbed](mbed.org). More information about libraries
and such can be found at the mbed website.

Getting Started
===============
+ [Create an account](https://developer.mbed.org/account/signup/?next=%2F) at mbed.
+ Install the [ST Link Driver](https://developer.mbed.org/teams/ST/wiki/ST-Link-Driver) on a Windows machine.
+ Upgrade the [firmware](https://developer.mbed.org/teams/ST/wiki/Nucleo-Firmware) on the Nucleo device.
+ Log in, open the mbed [online compiler](https://developer.mbed.org/compiler/) and build program 0 in the /src directory.
+ Upon compiling, the browser will begin downloading the binary executable generated for the Nucleo device. Copy this executable into the device. From this point on, it's not necessary to use a Windows machine to write code.
+ Some useful content can be found in the /utils directory.


Many sample programs can be found at the F411RE page of the mbed website as well,
but usually have to be composed to perform non-trivial tasks.
