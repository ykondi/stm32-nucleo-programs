import time
import sys
import uinput
import threading

mean = 140
events = None
device = None
xdif = ydif = zdif = 0

def writeThread():
	global device, xdif, ydif, zdif
	while 1:
		if zdif == 0:
			continue
		device.emit(uinput.REL_X, xdif)
		device.emit(uinput.REL_Y, ydif)
		time.sleep(0.008)

def main():
	global events, device
	global xdif, ydif, zdif
	events = (
        	uinput.REL_X,
        	uinput.REL_Y,
        	uinput.BTN_LEFT,
        	uinput.BTN_RIGHT,
        )

	device = uinput.Device(events)
	deviceThread = threading.Thread(target = writeThread)
	deviceThread.daemon = True
	deviceThread.start()

	while 1:
		vals = raw_input().split()
		if len(vals) < 3:
			continue
		x, y, zdif = float(vals[0]), float(vals[1]), float(vals[2])
		xdif = int((x - mean) / 10)
		ydif = int((y - mean) / 10)


if __name__ == "__main__":
	main()
