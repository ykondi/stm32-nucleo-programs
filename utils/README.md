Utilities
=========

This folder contains some useful utils to help with the programs in /src .

* install.sh: Sets up some basic requirements.
* acc_mouse_control.py : Program that controls mouse input. To be used in
conjuction with the multithreaded accelerometer program.
* nucleo : Copies argument into a connected nucleo device.
* nucleo_serial: Read values from nucleo serially. 
